<?php
    require_once('function.php');

    session_start();
    // URL de redirection par défaut (si pas d'action ou action non reconnue)
  $url_redirect = "../index.php";

  if (isset($_REQUEST['action'])) {
  
    if ($_REQUEST['action'] == 'authenticate') {
        /* ======== AUTHENT ======== */
        if (ipIsBanned($_SERVER['REMOTE_ADDR'])){
            // cette IP est bloquée
            $url_redirect = "connection.php?ipbanned";

        } else if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
            // manque login ou mot de passe
            $url_redirect = "connection.php?nullvalue";
            
        } else {
            $car_interdits = array("'", "\"", ";","%"); // une liste de caractères que je choisis d'interdire
            $utilisateur = findUserByLoginPwd(str_replace($car_interdits, "", $_REQUEST['login']), str_replace($car_interdits, "", $_REQUEST['mdp']), $_SERVER['REMOTE_ADDR']);
            
            if ($utilisateur == false) {
              // echec authentification
              $url_redirect = "connection.php?badvalue";
              
            } else {
              // authentification réussie
              $_SESSION["connected_user"] = $utilisateur;
              $_SESSION["listeUsers"] = findAllUsers();
              $url_redirect = "accueil.php";
            }
        }
        } else if ($_REQUEST['action'] == 'disconnect') {
        /* ======== DISCONNECT ======== */
        unset($_SESSION["connected_user"]);
        $url_redirect = $_REQUEST['loginPage'] ;
        
    }  else if ($_REQUEST['action'] == 'transfert') {
        /* ======== TRANSFERT ======== */
        if (!isset($_REQUEST['mytoken']) || $_REQUEST['mytoken'] != $_SESSION['mytoken']) {
            // echec vérification du token (ex : attaque CSRF)
            $url_redirect = "virement.php?err_token";
        } else {
            if (is_numeric ($_REQUEST['montant'])) {
                transfert($_REQUEST['destination'],$_SESSION["connected_user"]["numero_compte"], $_REQUEST['montant']);
                $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_REQUEST['montant'];
                $url_redirect = "virement.php?trf_ok";
                
            } else {
                $url_redirect = "virement.php?bad_mt=".$_REQUEST['montant'];
            }
        } 
        
    } else if ($_REQUEST['action'] == 'sendmsg') {
        /* ======== MESSAGE ======== */
        addMessage($_REQUEST['to'],$_SESSION["connected_user"]["id_user"],$_REQUEST['sujet'],$_REQUEST['corps']);
        $url_redirect = "accueil.php?msg_ok";
            
    } else if ($_REQUEST['action'] == 'msglist') {
        /* ======== MESSAGE ======== */
        $_SESSION['messagesRecus'] = findMessagesInbox($_REQUEST["userid"]);
        $url_redirect = "vw_messagerie.php";
            
    } 

     
}  

header("Location: $url_redirect");

?>
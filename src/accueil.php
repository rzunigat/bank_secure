<?php
  session_start();
  require_once('header.php');
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Accueil</title>
  <link rel="stylesheet" href="./css/style.css?v=<?php echo(rand()); ?>" />
</head>
<body>
    
    <section>

        <ul class="menu">
            <li>
                <a href="messagerie.php">Messagerie</a>
            </li>
            <li>
                <a href="Virement.php">Virement</a>
            </li>

            <?php 

                if (strcmp($_SESSION["connected_user"]["profil_user"], "Employee") == 0) {


                    echo "<li>
                                <a href='FicheClient.php'>Fiche Client</a>
                            </li>";
                }
                
            ?>
        </ul>

        <table id="customers">
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Compte</th>
                <th>Solde</th>
            </tr>

            <tr>
                <td><?php echo $_SESSION["connected_user"]["nom"]?></td>
                <td><?php echo $_SESSION["connected_user"]["prenom"]?></td>
                <td><?php echo $_SESSION["connected_user"]["numero_compte"]?></td>
                <td><?php echo $_SESSION["connected_user"]["solde_compte"]?></td>
            </tr>
         </table>
      
    </section>

</body>
</html>

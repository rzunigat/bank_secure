<?php

    require_once('include.php');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Bank Secure</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/style.css?v=<?php echo(rand()); ?>" />
</head>
<body>

    <div class="box">

        <header>
            <h1>Bank Secure</h1>
        </header>
    
        <section>
            <div class="login-page">
                <div>
                    <form class="form" method="POST" action="bankController.php">
                        <input type="hidden" name="action" value="authenticate">
                        <input type="text" name="login" class="elementForm" placeholder="login"/>
                        <input type="password" name="mdp" class="elementForm" placeholder="mot de passe"/>
                        <button id="myBtn" class="button">Login</button>
                    </form>
                </div>
            </div>

            <div class="center">
                <?php
                    if (isset($_REQUEST["nullvalue"])) {
                        echo '<p class="errmsg"> Please write your Login and Password ! </p>';
                    } else if (isset($_REQUEST["badvalue"])) {
                        echo '<p class="errmsg">Your Login or Password are incorrect ! </p>';
                    } else if (isset($_REQUEST["disconnect"])) {
                        echo '<p class="success"> User Disconnected !</p>';
                    }
                ?>
            </div>
        </section>

    </div>

</body>

<script>


</script>
</html>

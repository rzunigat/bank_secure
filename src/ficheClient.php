<?php
  session_start();
  require_once('header.php');
  require_once('function.php');
  require_once('include.php');

  // Verifier que l'utilisateur soit bien connecté
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {

    header('Location: ./connection.php');      
    exit();
  } 

  $clients = searchAllClients();
  
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Fiche Client</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/style.css" />
</head>
<body>
    
    <section>

       <table id="customers">
              <tr><th>Nom</th><th>Prenom</th><th>Numero Compte</th><th>Solde Compte</th></tr>
              <?php
              foreach ($clients as $cle => $client) {
                echo '<tr>';
                echo '<td>'.$client['nom'].'</td>';
                echo '<td>'.$client['prenom'].'</td>';
                echo '<td>'.$client['numero_compte'].'</td>';
                echo '<td>'.$client['solde_compte'].'</td>';
                echo '</tr>';
              }
               ?>
       </table>
      
    </section>

</body>
</html>

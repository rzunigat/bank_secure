<?php
  session_start();
  require_once('header.php');
  require_once('include.php');

    // Verifier que un utilisateur est bien connecté dans chaque page
    if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {

      header('Location: ./connection.php');      
      exit();
    } 

    $mytoken = bin2hex(random_bytes(128)); // token qui va servir à prévenir des attaques CSRF 
    $_SESSION["mytoken"] = $mytoken;

?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Virement</title>
  <link rel="stylesheet" type="text/css" media="all"  href="css/style.css" />
</head>
<body>
    
    <section>

    <input type="hidden" value=<?php echo $_SESSION["connected_user"]["mot_de_passe"] ?> id="passwd">

    <article>
            <form method="POST" action="bankController.php">
              <input type="hidden" name="action" value="transfert">
              <input type="hidden" name="mytoken" value="<?php echo $mytoken; ?>">
              <div class="fieldset">
                  <div class="fieldset_label">
                      <h1>Transférer de l'argent</h1>
                  </div>
                  <div class="field">
                      <label>N° compte destinataire : </label><input type="text" size="20" name="destination">
                  </div>
                  <div class="field">
                      <label>Montant à transférer : </label><input type="text" size="10" name="montant">
                  </div>
                  <button type="submit" value="Transferer" class="form-btn" onclick="send()"> </button>
                  <?php
                    if (isset($_REQUEST["err_token"])) {
                      echo '<p>Echec virement : le contrôle d\'intégrité a échoué.</p>';
                    }
                    if (isset($_REQUEST["trf_ok"])) {
                      echo '<p>Virement effectué avec succès.</p>';
                    }
                    if (isset($_REQUEST["bad_mt"])) {
                      echo '<p>Le montant saisi est incorrect : '.htmlentities($_REQUEST["bad_mt"], ENT_QUOTES).'</p>';
                    }
                  ?>
              </div>
            </form>
            </article>
      
    </section>

<script src="./js/virement.js"></script> 

</body>
</html>

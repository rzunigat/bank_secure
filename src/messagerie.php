<?php
  session_start();
  require_once('header.php');
  require_once('function.php');
  require_once('include.php');

  // Verifier que un utilisateur est bien connecté dans chaque page
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {

    header('Location: ./connection.php');      
    exit();
  } 

  $MyMessages = NULL;
  $MyMessages = findMessagesInbox($_SESSION["connected_user"]["id_user"]);
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messagerie</title>
  <link rel="stylesheet" href="./css/style.css?v=<?php echo(rand()); ?>" />
</head>
<body>
    
    <section>
  
       <article>
            <form method="POST" action="bankController.php">
              <input type="hidden" name="action" value="sendmsg">
              <div class="fieldset">
                  <div class="fieldset_label">
                      <h1>Envoyer un message</h1>
                  </div>
                  <div class="field">
                      <label>Destinataire : </label>
                      <select name="to">
                        <?php
                        foreach ($_SESSION['listeUsers'] as $id => $user) {
                          echo '<option value="'.$id.'">'.$user['nom'].' '.$user['prenom'].'</option>';
                        }
                        ?>
                      </select>
                  </div>
                  <div class="field">
                      <label>Sujet : </label><input type="text" size="20" name="sujet">
                  </div>
                  <div class="field">
                      <label>Message : </label><textarea name="corps" cols="25" rows="3""></textarea>
                  </div>
                  <input type="submit" value="Send" class="form-btn">
                  <?php
                  if (isset($_REQUEST["msg_ok"])) {
                    echo '<p>Message envoyé avec succès.</p>';
                  }
                  ?>
              </div>
            </form>
          </article>

          <article>
        
          <div class="liste">
            <table id="customers">
              <tr><th>Expéditeur</th><th>Sujet</th><th>Message</th></tr>
              <?php
              foreach ($MyMessages as $cle => $message) {
                echo '<tr>';
                echo '<td>'.$message['nom'].' '.$message['prenom'].'</td>';
                echo '<td>'.htmlentities($message['sujet_msg'], ENT_QUOTES).'</td>';
                echo '<td>'.htmlentities($message['corps_msg'], ENT_QUOTES).'</td>';
                echo '</tr>';
              }
               ?>
            </table>
          </div>
    
        </article>
      
    </section>

</body>
</html>

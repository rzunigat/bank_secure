CREATE DATABASE IF NOT EXISTS devoir3_sr03;
USE devoir3_sr03;

CREATE TABLE IF NOT EXISTS connection_errors(

    ip VARCHAR(15) NOT NULL,
    error_date VARCHAR(20) NOT NULL

);

CREATE TABLE IF NOT EXISTS Users (
	
	id_user int AUTO_INCREMENT,
	login VARCHAR(20) NOT NULL UNIQUE,
	mot_de_passe VARCHAR(30) NOT NULL,
    profil_user VARCHAR(10) CHECK (profil_user IN ('Client', 'Employee')),
	nom VARCHAR(20) NOT NULL,
	prenom VARCHAR(20) NOT NULL,
    numero_compte VARCHAR(23) NOT NULL,
    solde_compte DECIMAL(9,4) DEFAULT 0,
    PRIMARY KEY(id_user)

);

CREATE TABLE IF NOT EXISTS Messages (

    id_msg int AUTO_INCREMENT,
    id_user_to int REFERENCES Users(id_user),
    id_user_from int REFERENCES Users(id_user),
    sujet_msg VARCHAR(30),
    corps_msg VARCHAR(200),
    PRIMARY KEY (id_msg)

);



INSERT INTO Users(login, mot_de_passe, profil_user, nom, prenom, numero_compte, solde_compte) VALUES ("admin", "admin", "Employee", "AdminNom", "AdminPrenom", "12457864585523457841117", 999999.00);
INSERT INTO Users(login, mot_de_passe, profil_user, nom, prenom, numero_compte, solde_compte) VALUES ("jperez", "jperez", "Client", "Juan", "Perez", "12345678912345678912345", 100.00);